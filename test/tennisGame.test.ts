import TennisGame from '../lib/TennisGame';

describe('TennisGame', () => {
  it('gives love all when initialized', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');

    expect(tennisGame.getScore()).toBe('Love all');
  });

  it('increments points', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');
    tennisGame.wonPoint('Nadal');

    expect(tennisGame.getScore()).toBe('Fifteen - Love');
  });

  it('shows draw', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');

    expect(tennisGame.getScore()).toBe('Thirteen all');
  });

  it('shows deuce', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');

    expect(tennisGame.getScore()).toBe('Deuce');
  });

  it('returns to deuce from advantage', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Federer');

    expect(tennisGame.getScore()).toBe('Deuce');
  });

  it('shows advantage', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');

    expect(tennisGame.getScore()).toBe('Advantage Federer');
  });

  it('shows a winner', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');

    expect(tennisGame.getScore()).toBe('Win Federer');
  });

  it('shows a winner from deuce and advantage', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Federer');
    tennisGame.wonPoint('Nadal');
    tennisGame.wonPoint('Nadal');

    expect(tennisGame.getScore()).toBe('Win Nadal');
  });

  it('throws an error when player name doesnt exist', () => {
    const tennisGame = new TennisGame('Nadal', 'Federer');

    expect(() => {
      tennisGame.wonPoint('Aitor');
    }).toThrowError('Player not found');
  });
});
