import Referee from '../lib/Referee';

describe('Referee', () => {
    it('returns null when asked for winner in an unended match', () => {
        const referee = new Referee('Nadal', 'Federer');

        expect(referee.getWinnerName()).toBeNull();
    });
});
