import Scoreboard from './ScoreBoard';
import Referee from './Referee';

export default class TennisGame {
  private scoreBoard: Scoreboard;
  private referee: Referee;

  constructor(player1Name: string, player2Name: string) {
    this.referee = new Referee(player1Name, player2Name);
    this.scoreBoard = new Scoreboard(this.referee);
  }

  wonPoint = (playerName: string): void => {
    if (this.referee.hasWinner()) {
      return;
    }
    this.referee.wonPoint(playerName);
  }

  getScore = (): string => {
    return this.scoreBoard.getScore();
  }

  isFinished = (): boolean => {
    return this.referee.hasWinner();
  }
}
