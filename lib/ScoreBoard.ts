import Referee from './Referee';
import { Score } from './Score';

export default class Scoreboard {
  private referee: Referee;

  constructor(referee: Referee) {
    this.referee = referee;
  }
  getScore = (): string => {
    if (this.referee.hasWinner()) {
      return this.getWinnerMessage();
    }

    if (this.referee.hasAdvantage()) {
      return this.getAdvantageMessage();
    }

    if (this.referee.hasDeuce()) {
      return this.getDeuceMessage();
    }

    if (this.referee.hasDraw()) {
      return this.getDrawMessage();
    }

    return this.getDefaultScoreMessage();
  }

  private getWinnerMessage = (): string => {
    const winnerName = this.referee.getWinnerName();
    return `Win ${winnerName}`;
  }

  private getAdvantageMessage = (): string => {
    const advantageName = this.referee.getAdvantagePlayerName();
    return `Advantage ${advantageName}`;
  }

  private getDeuceMessage = (): string => {
    return 'Deuce';
  }

  private getDrawMessage = (): string => {
    const score: Score = this.referee.getDrawScore();
    return `${score} all`;
  }

  private getDefaultScoreMessage = (): string => {
    const player1Score = this.referee.getPlayer1Score();
    const player2Score = this.referee.getPlayer2Score();
    return `${player1Score} - ${player2Score}`;
  }
}
