import Player from './Player';
import { Score } from './Score';

export default class Referee {
  private player1: Player;
  private player2: Player;

  constructor(player1Name: string, player2Name: string) {
    this.player1 = new Player(player1Name);
    this.player2 = new Player(player2Name);
  }

  wonPoint = (playerName: string): void => {
    if (!this.existPlayer(playerName)) {
      throw new Error('Player not found');
    }

    if (this.hasAdvantage()) {
      this.manageAdvantageSituationForPlayer(playerName);
      return;
    }

    if (this.hasDeuce()) {
      this.giveAdvantageToPlayer(playerName);
      return;
    }

    this.increaseScoreToPlayer(playerName);
  }

  hasWinner = (): boolean => {
    return this.player1.isWinner() || this.player2.isWinner();
  }

  hasAdvantage = (): boolean => {
    return this.player1.isAdvantage() || this.player2.isAdvantage();
  }

  hasDeuce = (): boolean => {
    return this.player1.isForty() && this.player2.isForty();
  }

  hasDraw = (): boolean => {
    return this.player1.getScore() === this.player2.getScore();
  }

  getAdvantagePlayerName = (): string => {
    return this.player1.isAdvantage()
      ? this.player1.getName()
      : this.player2.getName();
  }

  getDrawScore = (): Score => {
    return this.player1.getScore();
  }

  getPlayer1Score = (): Score => {
    return this.player1.getScore();
  }

  getPlayer2Score = (): Score => {
    return this.player2.getScore();
  }

  getWinnerName = (): string | null => {
    if (!this.hasWinner()) {
      return null;
    }

    return this.player1.isWinner()
      ? this.player1.getName()
      : this.player2.getName();
  }

  private existPlayer = (playerName: string): boolean => {
    return this.player1.is(playerName) || this.player2.is(playerName);
  }

  private manageAdvantageSituationForPlayer = (playerName: string): void => {
    if (this.player1.is(playerName) && this.player1.isAdvantage()) {
      this.player1.giveWin();
      return;
    } else if (this.player2.is(playerName) && this.player2.isAdvantage()) {
      this.player2.giveWin();
      return;
    }
    this.setDeuce();
  }

  private giveAdvantageToPlayer = (playerName: string): void => {
    if (this.player1.is(playerName)) {
      this.player1.giveAdvantage();
      this.player2.giveForty();
    } else {
      this.player1.giveForty();
      this.player2.giveAdvantage();
    }
  }

  private increaseScoreToPlayer = (playerName: string): void => {
    if (this.player1.is(playerName)) {
      this.player1.increaseScore();
    } else {
      this.player2.increaseScore();
    }
  }

  private setDeuce = (): void => {
    this.player1.giveForty();
    this.player2.giveForty();
  }

}
