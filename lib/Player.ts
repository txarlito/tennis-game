import { Score } from './Score';

export default class Player {
  private name: string;
  private score: Score = Score.love;

  constructor(name: string) {
    this.name = name;
  }

  getName(): string {
    return this.name;
  }

  is = (name: string): boolean => {
    return this.name === name;
  }

  getScore = (): Score => {
    return this.score;
  }

  isWinner = (): boolean => {
    return this.score === Score.win;
  }

  isAdvantage = (): boolean => {
    return this.score === Score.advantage;
  }

  isForty = (): boolean => {
    return this.score === Score.forty;
  }

  isThirteen = (): boolean => {
    return this.score === Score.thirteen;
  }

  isFifteen = (): boolean => {
    return this.score === Score.fifteen;
  }

  giveLove = () => {
    this.score = Score.love;
  }

  giveFifteen = (): void => {
    this.score = Score.fifteen;
  }

  giveThirteen = (): void => {
    this.score = Score.thirteen;
  }

  giveForty = (): void => {
    this.score = Score.forty;
  }

  giveAdvantage = (): void => {
    this.score = Score.advantage;
  }

  giveWin = (): void => {
    this.score = Score.win;
  }

  increaseScore = (): void => {
    switch (this.score) {
      default:
      case Score.love:
        this.giveFifteen();
        break;
      case Score.fifteen:
        this.giveThirteen();
        break;
      case Score.thirteen:
        this.giveForty();
        break;
      case Score.forty:
        this.giveWin();
        break;
    }
  }
}
