export enum Score {
  love = 'Love',
  fifteen = 'Fifteen',
  thirteen = 'Thirteen',
  forty = 'Forty',
  advantage = 'Advantage',
  win = 'Win'
}
